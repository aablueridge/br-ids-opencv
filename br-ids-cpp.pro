TEMPLATE = app
TARGET = br-cpp-vision

CONFIG += debug_and_release console c++11 qml_debug

QT += core opengl multimedia core-private  gui-private quick
#sudo apt-get install qtmultimedia5-dev


#Things to install
#SSD Memory: https://medium.com/@ramin.nabati/installing-an-nvme-ssd-drive-on-nvidia-jetson-xavier-37183c948978
#EDGE TPU: https://coral.ai/docs/m2/get-started
#EDGE TPU COMPILER: https://coral.ai/docs/edgetpu/compiler/

CPR_DIR=/xavier_ssd/br-ids-opencv

# Include Curl for the People
LIBS += -L$$CPR_DIR/opt/cpr/build/lib/ -lcpr-d -lcurl-d
INCLUDEPATH += $$CPR_DIR/opt/cpr/include/cpr
DEPENDPATH += $$CPR_DIR/opt/cpr/include/cpr

# Edgetpu Runtime
EDGETPU_DIR=/xavier_ssd/edgetpu-master
INCLUDEPATH += $$EDGETPU_DIR
INCLUDEPATH += $$EDGETPU_DIR/libedgetpu
LIBS += -L$$EDGETPU_DIR/libedgetpu/direct/aarch64/ -l:libedgetpu.so.1.0


# TFLite Runtime
TF_DIR=/xavier_ssd/tensorflow
INCLUDEPATH += $$TF_DIR
INCLUDEPATH += $$TF_DIR/tensorflow/lite/tools/make/downloads/flatbuffers/include
INCLUDEPATH += $$TF_DIR/tensorflow/lite/tools/make/downloads/absl
LIBS += -L$$TF_DIR/tensorflow/lite/tools/make/gen/generic-aarch64_armv8-a/lib -ltensorflow-lite
LIBS += -lpthread -lm -ldl

# Benchmark Google
INCLUDEPATH += /xavier_ssd/benchmark-master/include

# UEYE Libary
LIBS +=  -lueye_api

##OPENCV
LIBS += -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_imgcodecs  -lopencv_videoio -lopencv_video -lopencv_tracking
INCLUDEPATH += /usr/local/include
INCLUDEPATH += /usr/local/include/opencv4


#OPENCV
#OPENCV_PATH=/usr/local
#LIBS += -L$$OPENCV_PATH/lib/ -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_imgcodecs  -lopencv_videoio -lopencv_video -lopencv_tracking
#INCLUDEPATH += /usr/local/include
#INCLUDEPATH += /usr/local/include/opencv
#INCLUDEPATH += /usr/local/include/opencv2

## DLIB
#INCLUDEPATH += /xavier_ssd/dlib
LIBS += -llapack -lcudnn -lpthread -ldlib -lopenblas
CONFIG += link_pkgconfig
PKGCONFIG += x11

#XML Libary
#sudo apt-get install libtinyxml-dev
LIBS += -ltinyxml
LIBS += -L/libs/tinyxml/lib
INCLUDEPATH += /libs/tinyxml/include/


copydata.commands = $(COPY_DIR) $$PWD/assets $$OUT_PWD
first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata


# GUI Sources and Headers
HEADERS += \
    src/cameralist.h \
    src/colormanager.h \
    src/eventthread.h \
    src/histogram.h \
    src/imageinfodlg.h \
    src/mainview.h \
    src/paintlabel.h \
    src/properties.h \
    src/qsliderex.h \
    src/qtui.h \
    src/version.h \
    src/edgetpu.h \
    src/trackdefect.h \
    src/mymodel.h
    #src/objectsrecogfilterrunable.h


SOURCES += \
    src/cameralist.cpp \
    src/colormanager.cpp \
    src/eventthread.cpp \
    src/histogram.cpp \
    src/imageinfodlg.cpp \
    src/main.cpp \
    src/mainview.cpp \
    src/paintlabel.cpp \
    src/properties.cpp \
    src/qsliderex.cpp \
    src/tabadvanced.cpp \
    src/tabcamera.cpp \
    src/tabformat.cpp \
    src/tabhotpixel.cpp \
    src/tabimage.cpp \
    src/tabio.cpp \
    src/tabprocessing.cpp \
    src/tabsize.cpp \
    src/tabsizexs.cpp \
    src/tabxs.cpp \
    src/trackdefect.cpp \
    src/mymodel.cpp
    #src/objectsrecogfilterrunable.cpp

# Tensorflor Sources and Headers
HEADERS += \
    src/auxutils.h \
    src/get_top_n.h \
    src/objectsrecogfilter.h \
    src/tensorflowlite.h

SOURCES += \
    src/auxutils.cpp \
    src/objectsrecogfilter.cpp \
    src/tensorflowlite.cpp

# Tracking Sources and Headers
HEADERS += \
    src/tracker/predictor/kalman/KalmanPredictor.h \
    src/tracker/predictor/particle/Particle.h \
    src/tracker/predictor/particle/ParticleFilter.h \
    src/tracker/predictor/particle/ParticlePredictor.h \
    src/tracker/predictor/Predictor.h \
    src/tracker/predictor/StationaryPredictor.h \
    src/tracker/Affinity.h \
    src/tracker/PAOT.h \
    src/tracker/RandomTracker.h \
    src/tracker/Tracker.h \
    src/util/BoundingBox.h \
    src/util/Detection.h \
    src/util/DetectionFileParser.h \
    src/util/Tracking.h

SOURCES += \
    src/tracker/predictor/kalman/KalmanPredictor.cpp \
    src/tracker/predictor/particle/Particle.cpp \
    src/tracker/predictor/particle/ParticleFilter.cpp \
    src/tracker/predictor/particle/ParticlePredictor.cpp \
    src/tracker/predictor/Predictor.cpp \
    src/tracker/predictor/StationaryPredictor.cpp \
    src/tracker/Affinity.cpp \
    src/tracker/PAOT.cpp \
    src/tracker/RandomTracker.cpp \
    src/tracker/Tracker.cpp \
    src/util/BoundingBox.cpp \
    src/util/Detection.cpp \
    src/util/DetectionFileParser.cpp \
    src/util/Tracking.cpp
#    ../dlib/dlib/all/source.cpp

FORMS += \
    src/histogram.ui \
    src/imageinfodlg.ui \
    src/mainview.ui \
    src/properties.ui \
    src/trackdefect.ui

RESOURCES += \
    mainview.qrc


#QMAKE_LFLAGS += -Wl,--allow-multiple-definition -Wl,--whole-archive
