#ifndef CPP_TRACKING_H
#define CPP_TRACKING_H


#include "BoundingBox.h"
#include <QString>
#include <ostream>
#include <string>
using namespace std;
struct Tracking {

    Tracking(int label, int ID, double conf, const BoundingBox &bb);
    Tracking(int label, int ID, double conf, string lname, string time, const BoundingBox &bb);
    Tracking(const Tracking &rhs);

    Tracking(Tracking &&rhs);

    // Prevent assignment
    Tracking &operator=(const Tracking &rhs) = delete;

    Tracking &operator=(Tracking &&rhs) = delete;

    const int label;
    const int ID;
    const double conf;
    const BoundingBox bb;
    const string lname;
    const string time;
};

std::ostream &operator<<(std::ostream &os, const Tracking &t);


#endif //CPP_TRACKING_H
