#include "Tracking.h"

// Constructors

Tracking::Tracking(int label, int ID, double conf, const BoundingBox &bb)
    : label(label), ID(ID), conf(conf), bb(bb) {}

Tracking::Tracking(int label, int ID, double conf, string lname, string time, const BoundingBox &bb)
    : label(label), ID(ID), conf(conf), lname(lname), time(time), bb(bb) {}

Tracking::Tracking(const Tracking &rhs)
        : label(rhs.label), conf(rhs.conf), ID(rhs.ID), bb(rhs.bb) {}

Tracking::Tracking(Tracking &&rhs)
        : label(std::move(rhs.label)), ID(std::move(rhs.ID)), conf(std::move(rhs.conf)), bb(std::move(rhs.bb)) {}

// Functions

std::ostream &operator<<(std::ostream &os, const Tracking &t) {
    os << "Label: " << t.label << " ID: " << t.ID << " Conf: " << t.conf << " " << t.bb;
    return os;
}
