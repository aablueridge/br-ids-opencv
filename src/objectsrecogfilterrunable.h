#ifndef OBJECTSRECOGFILTERRUNABLE_H
#define OBJECTSRECOGFILTERRUNABLE_H
//#include <QVideoFilterRunnable>
#include <QMutex>
#include <QElapsedTimer>
#include <QTimer>
#include <QMap>
#include <QImage>
#include <string>

#include <iostream>
#include <QStandardPaths>
#include <QRectF>
#include <QDir>
#include <QDebug>
#include <QFileDialog>
#include <QXmlStreamWriter>
#include "auxutils.h"


#include "tensorflowlite.h"
#include "tensorflowthread.h"
#include "objectsrecogfilter.h"

class ObjectsRecogFilterRunable /*: public QVideoFilterRunnable*/
{
public:
    ObjectsRecogFilterRunable();
    ObjectsRecogFilterRunable(ObjectsRecogFilter *filter);
    ObjectsRecogFilterRunable(ObjectsRecogFilter *filter, QStringList res);
    QImage detect(QImage input);
    void setResults(int net, QStringList res, QList<double> conf, QList<QRectF> box, QList<QImage> mask, int inftime);
    std::map<int, Tracking> trackingMap;
    void idTracker(QImage nobox, QImage box);
    std::map<int, Tracking> getTrackingMap();
    std::string time_secs();
    void SaveXMLFile(QString folder, QString file);
    //        void writeFile(QString folder, QString filename, QString data);
    bool checkDir(QString dirName);
    void createDir(QString dirName);
public slots:
    std::string getDay();
private:
    ObjectsRecogFilter *m_filter;
    int           network;
    QStringList   results;
    QList<double> confidence;
    QList<QRectF> boxes;
    double        infTime;
    QList<QImage> masks;
    QList<int> ids;
    int           inferenceTime;
    QElapsedTimer timer;
};

#endif // OBJECTSRECOGFILTERRUNABLE_H
