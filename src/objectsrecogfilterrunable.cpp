

//ObjectsRecogFilterRunable *ObjectsRecogFilter::createFilterRunnable()
//{
//    rfr = new ObjectsRecogFilterRunable(this,tf.getResults());
//    return rfr;
//}


ObjectsRecogFilterRunable::ObjectsRecogFilterRunable(ObjectsRecogFilter *filter, QStringList res)
{
    m_filter   = filter;
    results    = res;
}

ObjectsRecogFilterRunable::ObjectsRecogFilterRunable(ObjectsRecogFilter *filter)
{
    m_filter   = filter;
}

void ObjectsRecogFilterRunable::setResults(int net, QStringList res, QList<double> conf, QList<QRectF> box, QList<QImage> mask, int inftime)
{
    network       = net;
    results       = res;
    confidence    = conf;
    boxes         = box;
    masks         = mask;
    inferenceTime = inftime;
}

std::map<int, Tracking> ObjectsRecogFilterRunable::getTrackingMap()
{
    return trackingMap;
}

QImage ObjectsRecogFilterRunable::detect(QImage img)
{

    bool BGRVideoFrame = false;


    // Content size
    QString text = "";

    // If not initialized, intialize with image size
    if (!m_filter->getInitialized())
    {
        m_filter->init(img.height(),img.width());
        results.clear();
        confidence.clear();
        boxes.clear();
        ids.clear();
        infTime = -1;
    }
    else if (m_filter->getImgHeight() != img.height() ||
             m_filter->getImgWidth()  != img.width()){

        // If image size changed, initialize input tensor
        m_filter->initInput(img.height(),img.width());
    }
    m_filter->TensorFlowExecution(img);

    results = m_filter->getResults();
    confidence = m_filter->getConf();
    boxes =m_filter->getBoxes();
    ids = m_filter->getIds();
    network = 2;

    // Image classification network
    if (network == TensorFlowLite::knIMAGE_CLASSIFIER)
    {
        // Get current TensorFlow outputs
        QString objStr = results.count()>0    ? results.first()    : "";
        double  objCon = confidence.count()>0 ? confidence.first() : -1;

        // Check if there are results & the minimum confidence level is reached
        if (objStr.length()>0 && objCon >= m_filter->getMinConfidence())
        {
            // Formatting of confidence value
            QString confVal = QString::number(objCon * 100, 'f', 2) + " %";

            // Text
            text = objStr + '\n' + confVal + '\n';
        }
    }
    // Object detection network
    else if (network == TensorFlowLite::knOBJECT_DETECTION)
    {
        QRectF  srcRect;
        bool    showInfTime  = 1;

        // Calculate source rect if needed
        if (showInfTime)
            srcRect = AuxUtils::frameMatchImg(img, m_filter->getContentSize());
        // Draw masks on image
        if (!masks.isEmpty())
            img = AuxUtils::drawMasks(img,img.rect(),results,confidence,boxes,masks,m_filter->getMinConfidence(),m_filter->getActiveLabels());

        // Draw boxes on image
        img = AuxUtils::drawBoxes(img,img.rect(),results,confidence,boxes,ids,m_filter->getMinConfidence(),
                                  m_filter->getActiveLabels(),!BGRVideoFrame);

        // Show inference time
        if (showInfTime)
        {
            int new_time = m_filter->getInf();
            QString text = QString::number(new_time) + " ms";
            //std::cout <<"Time :" << new_time << std::endl;
            img = AuxUtils::drawText(img,srcRect,text);
        }
    }
    // NOTE: for BGR images loaded as RGB
    //    if (BGRVideoFrame) img = img->rgbSwapped();

    // Return video frame from img
    return img;
}

void ObjectsRecogFilterRunable::idTracker(QImage nobox, QImage box){


    for(int i=0;i<boxes.count();i++)
    {
        // Check if ID is not in map.
        if (!trackingMap.count(ids[i])){

            QString time = QString::fromStdString(time_secs());
            QString dir = QString::fromStdString(getDay());
            BoundingBox bb(boxes[i].left(), boxes[i].top(), boxes[i].width(), boxes[i].height());
            std::string lname = results[i].toStdString();

            std::cout << "Lname: " << lname << std::endl;
            Tracking d(1, ids[i], confidence[i], lname, time.toStdString(),  bb);
            trackingMap.insert( std::pair<int,Tracking>(ids[i],d) );
            std::cout << "Added: " << std::endl;

            // Check date

            if(!checkDir(dir)){
                createDir(dir);
            }
            nobox.save(dir+"/"+time+"_nobox.jpg");

            box.save(dir+"/"+time+".jpg");

            // Write XML

            QFile file(dir+"/"+time+"_nobox.xml");
            file.open(QIODevice::WriteOnly);

            QXmlStreamWriter xmlWriter(&file);
            xmlWriter.setAutoFormatting(true);

            xmlWriter.writeStartElement("annotation");
            xmlWriter.writeTextElement("folder", dir);
            xmlWriter.writeTextElement("filename", time+"_nobox.jpg" );
            xmlWriter.writeTextElement("path", "" );

            xmlWriter.writeStartElement("source");
            xmlWriter.writeTextElement("database", "Unknown");
            xmlWriter.writeEndElement(); //source

            //TODO Need
            xmlWriter.writeStartElement("size");
            xmlWriter.writeTextElement("width", QString::number(nobox.width()));
            xmlWriter.writeTextElement("height", QString::number(nobox.height()));
            xmlWriter.writeTextElement("depth", "3");
            xmlWriter.writeEndElement(); //size

            xmlWriter.writeTextElement("segmented", "0" );
            xmlWriter.writeStartElement("object");
            xmlWriter.writeTextElement("name",  results[i]);
            xmlWriter.writeTextElement("pose", "Unspecified");
            xmlWriter.writeTextElement("truncated", "0");
            xmlWriter.writeTextElement("difficult", "0");
            xmlWriter.writeStartElement("bndbox");
            xmlWriter.writeTextElement("xmin", QString::number(boxes[i].left()));
            xmlWriter.writeTextElement("ymin", QString::number(boxes[i].top()));
            xmlWriter.writeTextElement("xmax", QString::number(boxes[i].right()));
            xmlWriter.writeTextElement("ymax", QString::number(boxes[i].bottom()));
            xmlWriter.writeEndElement();//bndbox
            xmlWriter.writeEndElement();//object
            xmlWriter.writeEndElement();//object
            // Write CSV
        }
    }
}

std::string ObjectsRecogFilterRunable::time_secs(){
        time_t currentTime;
        struct tm *localTime;

        time(&currentTime);
        localTime = localtime(&currentTime);
        int Day = localTime->tm_mday;
        int Month = localTime->tm_mon + 1;
        int Year = localTime->tm_year + 1900;
        int Hour = localTime->tm_hour;
        int Min = localTime->tm_min;
        int Sec = localTime->tm_sec;

        std::string dateTime = std::to_string(Year)
                + std::to_string(Month)
                + std::to_string(Day) + "_"
                + std::to_string(Hour)
                + std::to_string(Min)
                + std::to_string(Sec);


        return dateTime;
        //TODO save dir
        //DirectoryInfo directoryR = Directory::CreateDirectory("C:\\Users\\Desktop\\" + dateTime);
}

//TODO Pass in xy
void ObjectsRecogFilterRunable::SaveXMLFile(QString folder, QString filename)
{

    QFile file(folder+"/"+filename+".xml");
    file.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&file);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();

    xmlWriter.writeStartElement("annotation");
    xmlWriter.writeTextElement("folder", "" );
    xmlWriter.writeTextElement("filename", "" );
    xmlWriter.writeTextElement("path", "" );

    xmlWriter.writeStartElement("source");
    xmlWriter.writeTextElement("database", "Unknown");
    xmlWriter.writeEndElement(); //source

    //TODO Need
    xmlWriter.writeStartElement("size");
    xmlWriter.writeTextElement("width", QString::number(m_filter->getImgWidth()));
    xmlWriter.writeTextElement("height", QString::number(m_filter->getImgHeight()));
    xmlWriter.writeTextElement("depth", "3");
    xmlWriter.writeEndElement(); //size

    xmlWriter.writeTextElement("segmented", "0" );


    xmlWriter.writeStartElement("object");
    xmlWriter.writeTextElement("name", "defect");
    xmlWriter.writeTextElement("pose", "Unspecified");
    xmlWriter.writeTextElement("truncated", "0");
    xmlWriter.writeTextElement("difficult", "0");
    xmlWriter.writeStartElement("bndbox");
    xmlWriter.writeTextElement("xmin", "");
    xmlWriter.writeTextElement("ymin", "");
    xmlWriter.writeTextElement("xmax", "");
    xmlWriter.writeTextElement("ymax", "");
    xmlWriter.writeEndElement();//bndbox
    xmlWriter.writeEndElement();//object

    xmlWriter.writeEndElement(); //annotation


    file.close();
}

//void ObjectsRecogFilterRunable::writeFile(QString folder, QString filename, QString data){

//    QFile file(folder + "/" + filename);
//    file.open(QIODevice::ReadWrite | QIODevice::Text);
//    file.write(&data);
//    file.close();

//    if(file.exists())
//    {
//        qDebug () << "file exists" << endl;
//        //QString data =  file.reAll();
//        //qDebug () << "data in file:" << data << endl;
//        qDebug()<<"file already created"<<endl;
//    }
//    else
//    {
//        qDebug () << "file does not exists" << endl;
//        qDebug()<<"file created"<<endl;
//    }
//    file.close();


//}


bool ObjectsRecogFilterRunable::checkDir(QString dirName){
    //To check if a directory named "Folder" exists use:
    QDir(dirName).exists();
}

void ObjectsRecogFilterRunable::createDir(QString dirName){
    //To create a new folder named "MyFolder" use:
   QDir().mkdir(dirName);
}

std::string ObjectsRecogFilterRunable::getDay(){
        time_t currentTime;
        struct tm *localTime;

        time(&currentTime);
        localTime = localtime(&currentTime);
        int Day = localTime->tm_mday;
        int Month = localTime->tm_mon + 1;
        int Year = localTime->tm_year + 1900;
        int Hour = localTime->tm_hour;
        int Min = localTime->tm_min;
        int Sec = localTime->tm_sec;

        std::string dateTime = std::to_string(Year) + "_"
                + std::to_string(Month) + "_"
                + std::to_string(Day);
        return dateTime;
        //TODO save dir
        //DirectoryInfo directoryR = Directory::CreateDirectory("C:\\Users\\Desktop\\" + dateTime);
}
