#ifndef AUXUTILS_H
#define AUXUTILS_H

#include <QObject>
#include <QQuickItem>
#include <QImage>
#include <QVideoFrame>
#include <QStandardPaths>
#include <QMutex>
#include <QTimer>

const QString assetsPath = "./assets";
//const QString modelName  = "mobilenet_ssd_v1_coco_quant_postprocess_edgetpu.tflite";
const QString modelName  = "mobilenet_ssd_v2_coco_quant_postprocess_edgetpu.tflite";
const QString labelsName = "labelmap.txt";
const QString RES_CHAR   = "x";

class AuxUtils
{
public:

    static void deleteFile(QString filename);

    static QString urlToFile(QString path);
    static bool copyFile(QString origin, QString destination);

    static bool fileExist(QString filename);
    static QString resolveModelFilePath(QString modelName);
    static QImage drawText(QImage image, QRectF rect, QString text, Qt::AlignmentFlag pos = Qt::AlignBottom,
                           Qt::GlobalColor borderColor = Qt::black,
                           double borderSize = 0.5,
                           Qt::GlobalColor fontColor = Qt::white,
                           QFont font = QFont("Times", 16, QFont::Bold));
    static QImage drawBoxes(QImage image, QRect rect, QStringList captions, QList<double> confidences, QList<QRectF> boxes, QList<int> ids, double minConfidence,
                            QMap<QString, bool> activeLabels, bool rgb);
    static QImage drawMasks(QImage image, QRect rect, QStringList captions, QList<double> confidences, QList<QRectF> boxes, QList<QImage> masks, double minConfidence, QMap<QString, bool> activeLabels);
    static QString getDefaultModelFilename();
    static QString getDefaultLabelsFilename();
    static QRectF frameMatchImg(QImage img, QSize rectSize);
    static int sp(int pixel, QSizeF size);
    static double dpi(QSizeF size);
    static QString deviceInfo();
    static QString qtVersion();
    static QString getAssetsPath();
    static QImage setOpacity(QImage& image, qreal opacity);
    static bool isBGRvideoFrame(QVideoFrame f);
    static bool isBGRimage(QImage i);
    bool readLabels(QString filename);
    QStringList getLabels();
    int numberThreads();
    static QVariantList networkInterfaces();
    static void setAngleHor(double angle);
    static void setAngleVer(double angle);
    static bool setResolution(QString res);
    void imageSaved(QString file);
    static double   angleHor;
    static double   angleVer;
    static int      width;
    static int      height;

private:
    static QString copyIfNotExistOrUpdate(QString file, QString defFile);
    static QByteArray fileMD5(QString filename);

    // Constant values
    static constexpr int     FONT_PIXEL_SIZE_TEXT = 40*2;
    static constexpr int     FONT_PIXEL_SIZE_BOX  = 43*3;
    static constexpr double  MASK_OPACITY         = 0.6;
    static constexpr double  LINE_WIDTH           = 9;
    static constexpr int     FONT_HEIGHT_MARGIN   = 9;
    static constexpr int     FONT_WIDTH_MARGIN    = 9;
    QStringList labels;

    // Segmentation
    static const int MAX_INTENSITY = 255;
    static QVector<int> histogram(QImage img);
    static QImage segmentation(QImage img, QVector<int> histogram, QColor color, int manual_threshold=-1);



};

#endif // AUXUTILS_H
