#include "trackdefect.h"
#include "ui_trackdefect.h"


#include <QPixmap>
#include <QPainter>
#include <QPen>
#include <QtCore/qmath.h>
#include <QMessageBox>
#include <QMap>
#include "PAOT.h"
#include <map>

TrackDefect::TrackDefect(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TrackDefect)
{
    ui->setupUi(this);
    connect(ui->pushButtonCloseTrack, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->pushButtonCloseTrack, SIGNAL(clicked()), parent, SLOT(closeTrackDefect()));
    setWindowTitle("Defect Tracker");
}

//RETURN take picture and name
void TrackDefect::updateTrack(std::map<int, Tracking> trackingMap){

    int rows = trackingMap.size();

    int cols =8 ;
    model =  new QStandardItemModel(rows,cols,this);
    ui->tableView->setModel(model);
    model->setHeaderData(0, Qt::Horizontal, QObject::tr("ID"));
    model->setHeaderData(1, Qt::Horizontal, QObject::tr("Class"));
    model->setHeaderData(2, Qt::Horizontal, QObject::tr("Image Name"));
    model->setHeaderData(3, Qt::Horizontal, QObject::tr("Time"));
    model->setHeaderData(4, Qt::Horizontal, QObject::tr("x1"));
    model->setHeaderData(5, Qt::Horizontal, QObject::tr("y1"));
    model->setHeaderData(6, Qt::Horizontal, QObject::tr("width"));
    model->setHeaderData(7, Qt::Horizontal, QObject::tr("height"));

    int row =0;
    for (auto const &t : trackingMap){

        try{
            //QString lname = t.second.lname;
            std::cout << "Lname: " << t.second.lname << std::endl;
            int id = t.second.ID;

            model->setData(model->index(row,0,QModelIndex()), id);
            model->setData(model->index(row,1,QModelIndex()), t.second.label);
            model->setData(model->index(row,2,QModelIndex()), "lname");
            model->setData(model->index(row,3,QModelIndex()), QString::fromStdString(t.second.time)); //TODO SET TIME
            model->setData(model->index(row,4,QModelIndex()), QString::number(int(t.second.bb.cx)));
            model->setData(model->index(row,5,QModelIndex()), QString::number(int(t.second.bb.cy)));
            model->setData(model->index(row,6,QModelIndex()), QString::number(int(t.second.bb.width)));
            model->setData(model->index(row,7,QModelIndex()), QString::number(int(t.second.bb.height)));
        }
        catch(...){
            std::cout << "Error" << std::endl;
        }
        row++;
    }
}

TrackDefect::~TrackDefect()
{
    delete ui;
}


void TrackDefect::saveCSV(){
    QString textData;
    int rows = model->rowCount();
    int columns = model->columnCount();

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {

                textData += model->data(model->index(i,j)).toString();
                textData += ", ";     // for .csv file format
        }

        textData += "\n";             // (optional: for new line segmentation)
    }

    // [Save to file] (header file <QFile> needed)
    // .csv
    QFile csvFile(QString::fromStdString(time_secs())+".csv");
    if(csvFile.open(QIODevice::WriteOnly | QIODevice::Truncate)) {

        QTextStream out(&csvFile);
        out << textData;

        csvFile.close();
    }

    // .txt
//    QFile txtFile("test.txt");
//    if(txtFile.open(QIODevice::WriteOnly | QIODevice::Truncate)) {

//        QTextStream out(&txtFile);
//        out << textData;

//        txtFile.close();
//    }
}

std::string TrackDefect::time_secs(){
        time_t currentTime;
        struct tm *localTime;

        time(&currentTime);
        localTime = localtime(&currentTime);
        int Day = localTime->tm_mday;
        int Month = localTime->tm_mon + 1;
        int Year = localTime->tm_year + 1900;
        int Hour = localTime->tm_hour;
        int Min = localTime->tm_min;
        int Sec = localTime->tm_sec;

        std::string dateTime = std::to_string(Day) + "."
                + std::to_string(Month) + "."
                + std::to_string(Year) + " ("
                + std::to_string(Hour) + "."
                + std::to_string(Min) + "."
                + std::to_string(Sec) + ")";

        return dateTime;
        //TODO save dir
        //DirectoryInfo directoryR = Directory::CreateDirectory("C:\\Users\\Desktop\\" + dateTime);
}

