#ifndef OBJECTSRECOGFILTER_H
#define OBJECTSRECOGFILTER_H

//#include <QVideoFilterRunnable>
#include <QMutex>
#include <QElapsedTimer>
#include <QTimer>
#include <QMap>
#include <QImage>
#include "tensorflowlite.h"
#include "tensorflowthread.h"
#include <string>

#include <iostream>
#include <QStandardPaths>
#include <QJsonObject>
#include <QJsonDocument>
#include <QRectF>
#include <QDir>
#include <QDebug>
#include <QFileDialog>
#include <QXmlStreamWriter>
#include "auxutils.h"
#include <cpr/cpr.h>

class ObjectsRecogFilter /*: public QAbstractVideoFilter*/
{
public slots:
    std::string getDay();
private:
    QMap<QString,bool>activeLabels;
    double minConf;
    bool   running;
    bool   initialized;
    bool   showInfTime;
    //int   infTime;
    QSize  videoSize;
    QString kindNetwork;
    TensorFlowLite tf;

    // OLD SHIT
    int           network;
    QStringList   results;
    QList<double> confidence;
    QList<QRectF> boxes;
    double        infTime;
    QList<QImage> masks;
    QList<int>    ids;
    int           inferenceTime;
    QElapsedTimer timer;

public:
    ObjectsRecogFilter();
    //~ObjectsRecogFilter();

    double getMinConfidence() const;
    void setMinConfidence(double value);
    QSize getContentSize() const;
    void setContentSize(const QSize &value);
    double getImgHeight() const;
    double getImgWidth() const;
    bool getInitialized() const;
    void setInitialized(bool value);
    QString getModel() const;
    void setModel(const QString &value, QString model_file, QString model_label);
    bool getShowTime() const;
    void setShowTime(bool value);
    void init(int imgHeight, int imgWidth);
    void initInput(int imgHeight, int imgWidth);
    void TensorFlowExecution(QImage imgTF);
    QList<QRectF> getBoxes();
    QStringList getResults();
    QList<double> getConf();
    QList<int> getIds();
    QMap<QString,bool> getActiveLabels();
    int getInf();
    bool getActiveLabel(QString key);
    void setActiveLabel(QString key, bool value);


    // OLD SHIT
    QImage detect(QImage input);
    std::map<int, Tracking> trackingMap;
    void idTracker(QImage nobox, QImage box);
    std::map<int, Tracking> getTrackingMap();
    std::string time_secs();
    void SaveXMLFile(QString folder, QString file);
    bool checkDir(QString dirName);
    void createDir(QString dirName);
};
#endif // OBJECTSRECOGFILTER_H
