#ifndef OBJECTSRECOGFILTER_H
#define OBJECTSRECOGFILTER_H

//#include <QVideoFilterRunnable>
#include <QMutex>
#include <QElapsedTimer>
#include <QTimer>
#include <QMap>
#include <QImage>
#include "tensorflowlite.h"
#include "tensorflowthread.h"
#include <string>

class ObjectsRecogFilterRunable;

class ObjectsRecogFilter /*: public QAbstractVideoFilter*/
{

private:
    QMap<QString,bool>activeLabels;
    double camOrientation;
    double vidOrientation;
    double minConf;
    bool   running;
    bool   initialized;
    bool   showInfTime;
    int   infTime;
    QMutex mutex;
    QSize  videoSize;
    QString kindNetwork;
    TensorFlowLite tf;
    //    TensorflowThread tft;
    ObjectsRecogFilterRunable *rfr;

public:
    ObjectsRecogFilter();
    ~ObjectsRecogFilter();

    ObjectsRecogFilterRunable *createFilterRunnable();
    double getMinConfidence() const;
    void setMinConfidence(double value);
    //    bool getRunning();
    void releaseRunning();
    QSize getContentSize() const;
    void setContentSize(const QSize &value);
    double getImgHeight() const;
    double getImgWidth() const;
    bool getInitialized() const;
    void setInitialized(bool value);
    QString getModel() const;
    void setModel(const QString &value, QString model_file, QString model_label);
    bool getShowTime() const;
    void setShowTime(bool value);
    bool getShowInfTime() const;
    void setShowInfTime(bool value);
    void init(int imgHeight, int imgWidth);
    void initInput(int imgHeight, int imgWidth);
    void setRunning(bool value);
    void TensorFlowExecution(QImage imgTF);
    void processResults(int network, QStringList res, QList<double> conf, QList<QRectF> boxes, QList<QImage> masks, int inftime);
    QList<QRectF> getBoxes();
    QStringList getResults();
    QList<double> getConf();
    QList<int> getIds();
    QMap<QString,bool> getActiveLabels();
    int getInf();
    bool getActiveLabel(QString key);
    void setActiveLabel(QString key, bool value);
    //    std::vector<Tracking> getTrackings();

};

class ObjectsRecogFilterRunable /*: public QVideoFilterRunnable*/
{
public:
    ObjectsRecogFilterRunable(ObjectsRecogFilter *filter);
    ObjectsRecogFilterRunable(ObjectsRecogFilter *filter, QStringList res);
    QImage detect(QImage input);
    void setResults(int net, QStringList res, QList<double> conf, QList<QRectF> box, QList<QImage> mask, int inftime);
    std::map<int, Tracking> trackingMap;
    void idTracker(QImage nobox, QImage box);
    std::map<int, Tracking> getTrackingMap();
    std::string time_secs();
    void SaveXMLFile(QString folder, QString file);
    //        void writeFile(QString folder, QString filename, QString data);
    bool checkDir(QString dirName);
    void createDir(QString dirName);
public slots:
    std::string getDay();
private:
    ObjectsRecogFilter *m_filter;
    int           network;
    QStringList   results;
    QList<double> confidence;
    QList<QRectF> boxes;
    double        infTime;
    QList<QImage> masks;
    QList<int> ids;
    int           inferenceTime;
    QElapsedTimer timer;
};

#endif // OBJECTSRECOGFILTER_H
