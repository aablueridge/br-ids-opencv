#ifndef TRACKDEFECT_H
#define TRACKDEFECT_H

#include <QDialog>
#include <QTableView>
#include <QItemDelegate>
#include <QStandardItemModel>
#include "PAOT.h"
#include <QFile>
#include "mymodel.h"
#include <string>
using namespace std;

namespace Ui {
class TrackDefect;
}

class TrackDefect : public QDialog
{
    Q_OBJECT

public:
    explicit TrackDefect(QWidget *parent = 0);
    ~TrackDefect();
    void updateTrack(std::map<int, Tracking> trackingMap);

    std::string time_secs();

public slots:
    void saveCSV();
private:
    Ui::TrackDefect *ui;
    // QStandardItemModel provides a classic
    // item-based approach to working with the model.
    QStandardItemModel *model;
};

#endif // TRACKDEFECT_H
