#include "objectsrecogfilter.h"

using namespace std;

ObjectsRecogFilter::ObjectsRecogFilter()
{
    initialized = false;
}

void ObjectsRecogFilter::init(int imgHeight, int imgWidth)
{
    initialized = tf.init(imgHeight,imgWidth);

}

void ObjectsRecogFilter::initInput(int imgHeight, int imgWidth)
{
    tf.initInput(imgHeight,imgWidth);
}

void ObjectsRecogFilter::TensorFlowExecution(QImage imgTF)
{
    tf.run(imgTF);
}

bool ObjectsRecogFilter::getInitialized() const
{
    return initialized;
}

void ObjectsRecogFilter::setInitialized(bool value)
{
    initialized = value;
}



double ObjectsRecogFilter::getMinConfidence() const
{
    return minConf;
}

void ObjectsRecogFilter::setMinConfidence(double value)
{
    minConf = value;
    tf.setThreshold(minConf);
}


QSize ObjectsRecogFilter::getContentSize() const
{
    return videoSize;
}

void ObjectsRecogFilter::setContentSize(const QSize &value)
{
    videoSize = value;
}


void ObjectsRecogFilter::setActiveLabel(QString key, bool value)
{
    activeLabels[key] = value;
}

QMap<QString,bool> ObjectsRecogFilter::getActiveLabels()
{
    return activeLabels;
}

bool ObjectsRecogFilter::getActiveLabel(QString key)
{
    return activeLabels.value(key,false);
}

double ObjectsRecogFilter::getImgHeight() const
{
    return tf.getHeight();
}

double ObjectsRecogFilter::getImgWidth() const
{
    return tf.getWidth();
}

QList<QRectF>  ObjectsRecogFilter::getBoxes()
{
    return tf.getBoxes();
}

QList<int>  ObjectsRecogFilter::getIds()
{
    return tf.getRIds();
}

QStringList ObjectsRecogFilter::getResults()
{
    return tf.getResults();
}

QList<double> ObjectsRecogFilter::getConf()
{
    return tf.getConfidence();
}


int ObjectsRecogFilter::getInf()
{
    return tf.getInferenceTime();
}

QString ObjectsRecogFilter::getModel() const
{
    return kindNetwork;
}

// TODO: set modelType literals as constant values,
void ObjectsRecogFilter::setModel(const QString &value, QString model_file, QString model_label)
{


    //Pass in model name and label file

    const QString MODEL_FILE_IMG_CLA  = "mobilenet_ssd_v2_coco_quant_postprocess_edgetpu.tflite";
    const QString MODEL_FILE_OBJ_DET  = model_file;
    const QString LABELS_FILE_IMG_CLA = "labelmap.txt";
    const QString LABELS_FILE_OBJ_DET = model_label;

    //QString assetsPath = AuxUtils::getAssetsPath();
    QString modelFilename;
    QString labelsFilename;
    kindNetwork = value;

    modelFilename  = kindNetwork == "ImageClassification" ? MODEL_FILE_IMG_CLA : MODEL_FILE_OBJ_DET;
    labelsFilename = assetsPath + QDir::separator() + (kindNetwork == "ImageClassification" ? LABELS_FILE_IMG_CLA : LABELS_FILE_OBJ_DET);

    tf.setFilename(AuxUtils::resolveModelFilePath(modelFilename));
    tf.setLabelsFilename(labelsFilename);
    initialized = false;
    tf.setTPU(true);
    //    emit initializedChanged(initialized);
    //    tft.setTf(&tf);
}

bool ObjectsRecogFilter::getShowTime() const
{
    return showInfTime;
}

void ObjectsRecogFilter::setShowTime(bool value)
{
    showInfTime = value;
}

std::map<int, Tracking> ObjectsRecogFilter::getTrackingMap()
{
    return trackingMap;
}
QImage ObjectsRecogFilter::detect(QImage img)
{

    bool BGRVideoFrame = false;


    // Content size
    QString text = "";

    // If not initialized, intialize with image size
    if (!getInitialized())
    {
        init(img.height(),img.width());
        results.clear();
        confidence.clear();
        boxes.clear();
        ids.clear();
        infTime = -1; //DOUBLE
    }
    else if (getImgHeight() != img.height() ||
             getImgWidth()  != img.width()){

        // If image size changed, initialize input tensor
        initInput(img.height(),img.width());
    }
    TensorFlowExecution(img);

    results = getResults();
    confidence = getConf();
    boxes = getBoxes();
    ids = getIds();
    network = 2;

    // Image classification network
    if (network == TensorFlowLite::knIMAGE_CLASSIFIER)
    {
        // Get current TensorFlow outputs
        QString objStr = results.count()>0    ? results.first()    : "";
        double  objCon = confidence.count()>0 ? confidence.first() : -1;

        // Check if there are results & the minimum confidence level is reached
        if (objStr.length()>0 && objCon >= getMinConfidence())
        {
            // Formatting of confidence value
            QString confVal = QString::number(objCon * 100, 'f', 2) + " %";

            // Text
            text = objStr + '\n' + confVal + '\n';
        }
    }
    // Object detection network
    else if (network == TensorFlowLite::knOBJECT_DETECTION)
    {
        QRectF  srcRect;
        bool    showInfTime  = 1;

        // Calculate source rect if needed
        if (showInfTime)
            srcRect = AuxUtils::frameMatchImg(img, getContentSize());
        // Draw masks on image
        if (!masks.isEmpty())
            img = AuxUtils::drawMasks(img,img.rect(),results,confidence,boxes,masks, getMinConfidence(), getActiveLabels());

        // Draw boxes on image
        img = AuxUtils::drawBoxes(img,img.rect(),results,confidence,boxes,ids, getMinConfidence(),
                                  getActiveLabels(),!BGRVideoFrame);

        // Show inference time
        if (showInfTime)
        {
            int new_time = getInf();
            QString text = QString::number(new_time) + " ms";
            //std::cout <<"Time :" << new_time << std::endl;
            img = AuxUtils::drawText(img,srcRect,text);
        }
    }
    // NOTE: for BGR images loaded as RGB
    //    if (BGRVideoFrame) img = img->rgbSwapped();

    // Return video frame from img
    return img;
}

void ObjectsRecogFilter::idTracker(QImage nobox, QImage box){


    for(int i=0;i<boxes.count();i++)
    {
        // Check if ID is not in map.
        if (!trackingMap.count(ids[i])){

            QString time = QString::fromStdString(time_secs());
            QString dir = QString::fromStdString(getDay());
            BoundingBox bb(boxes[i].left(), boxes[i].top(), boxes[i].width(), boxes[i].height());
            std::string lname = results[i].toStdString();

            std::cout << "Lname: " << lname << std::endl;
            Tracking d(1, ids[i], confidence[i], lname, time.toStdString(),  bb);
            trackingMap.insert( std::pair<int,Tracking>(ids[i],d) );
            std::cout << "Added: " << std::endl;

            // Check date

            if(!checkDir(dir)){
                createDir(dir);
            }
            QString noBoxName = dir+"/"+time+"_nobox.jpg";
            nobox.save(noBoxName);

            box.save(dir+"/"+time+".jpg");

            // Write XML
            QString noBoxXml = dir+"/"+time+"_nobox.xml";
            QFile file(noBoxXml);

            file.open(QIODevice::WriteOnly);
            QXmlStreamWriter xmlWriter(&file);
            xmlWriter.setAutoFormatting(true);
            xmlWriter.writeStartElement("annotation");
            xmlWriter.writeTextElement("folder", dir);
            xmlWriter.writeTextElement("filename", time+"_nobox.jpg" );
            xmlWriter.writeTextElement("path", "" );
            xmlWriter.writeStartElement("source");
            xmlWriter.writeTextElement("database", "Unknown");
            xmlWriter.writeEndElement(); //source

            //TODO Need
            xmlWriter.writeStartElement("size");
            xmlWriter.writeTextElement("width", QString::number(nobox.width()));
            xmlWriter.writeTextElement("height", QString::number(nobox.height()));
            xmlWriter.writeTextElement("depth", "3");
            xmlWriter.writeEndElement(); //size

            xmlWriter.writeTextElement("segmented", "0" );
            xmlWriter.writeStartElement("object");
            xmlWriter.writeTextElement("name",  results[i]);
            xmlWriter.writeTextElement("pose", "Unspecified");
            xmlWriter.writeTextElement("truncated", "0");
            xmlWriter.writeTextElement("difficult", "0");
            xmlWriter.writeStartElement("bndbox");
            xmlWriter.writeTextElement("xmin", QString::number(boxes[i].left()));
            xmlWriter.writeTextElement("ymin", QString::number(boxes[i].top()));
            xmlWriter.writeTextElement("xmax", QString::number(boxes[i].right()));
            xmlWriter.writeTextElement("ymax", QString::number(boxes[i].bottom()));
            xmlWriter.writeEndElement();//bndbox
            xmlWriter.writeEndElement();//object
            xmlWriter.writeEndElement();//object
            // Write CSV

            QString path = QDir::currentPath();
            QJsonObject json_obj;
            json_obj["width"]=  QString::number(nobox.width());
            json_obj["height"]=  QString::number(nobox.height());
            json_obj["xmin"]= QString::number(boxes[i].left());
            json_obj["ymin"]= QString::number(boxes[i].top());
            json_obj["xmax"]= QString::number(boxes[i].right());
            json_obj["ymax"]= QString::number(boxes[i].bottom());
            json_obj["path"]= path +"/"+dir;
            json_obj["jpg"]= time+"_nobox.jpg";

            QJsonDocument doc(json_obj);
            QString strJson(doc.toJson(QJsonDocument::Compact));
            //QString serverJson = QString("{\"data\":\"test\",\"path\":\"%1\",\"jpg\":\"%2\",\"xml\":\"%3\"}").arg(path).arg(noBoxName).arg(noBoxXml);
            QByteArray severData = strJson.toLocal8Bit();
            auto serverSend = cpr::Post(cpr::Url{"http://localhost:5000/post"},
                                        cpr::Body{severData.toStdString()},
                                        cpr::Timeout{2000},
                                        cpr::Header{{"Content-Type", "application/json; charset=utf-8"}});

            // Check for timeout of influxdb
            if (serverSend.elapsed <= 2){
                qDebug() << "Server Post Return: " << QString::fromStdString(serverSend.text)<< "\n" <<"Elapsed: " << serverSend.elapsed;
            }
            else{
                qDebug() << "Server HTTP Post Timeout: " << serverSend.elapsed;
            }
        }
    }

}

std::string ObjectsRecogFilter::time_secs(){
        time_t currentTime;
        struct tm *localTime;

        time(&currentTime);
        localTime = localtime(&currentTime);
        int Day = localTime->tm_mday;
        int Month = localTime->tm_mon + 1;
        int Year = localTime->tm_year + 1900;
        int Hour = localTime->tm_hour;
        int Min = localTime->tm_min;
        int Sec = localTime->tm_sec;

        std::string dateTime = std::to_string(Year)
                + std::to_string(Month)
                + std::to_string(Day) + "_"
                + std::to_string(Hour)
                + std::to_string(Min)
                + std::to_string(Sec);


        return dateTime;
        //TODO save dir
        //DirectoryInfo directoryR = Directory::CreateDirectory("C:\\Users\\Desktop\\" + dateTime);
}

//TODO Pass in xy
void ObjectsRecogFilter::SaveXMLFile(QString folder, QString filename)
{

    QFile file(folder+"/"+filename+".xml");
    file.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&file);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();

    xmlWriter.writeStartElement("annotation");
    xmlWriter.writeTextElement("folder", "" );
    xmlWriter.writeTextElement("filename", "" );
    xmlWriter.writeTextElement("path", "" );

    xmlWriter.writeStartElement("source");
    xmlWriter.writeTextElement("database", "Unknown");
    xmlWriter.writeEndElement(); //source

    //TODO Need
    xmlWriter.writeStartElement("size");
    xmlWriter.writeTextElement("width", QString::number(getImgWidth()));
    xmlWriter.writeTextElement("height", QString::number(getImgHeight()));
    xmlWriter.writeTextElement("depth", "3");
    xmlWriter.writeEndElement(); //size

    xmlWriter.writeTextElement("segmented", "0" );


    xmlWriter.writeStartElement("object");
    xmlWriter.writeTextElement("name", "defect");
    xmlWriter.writeTextElement("pose", "Unspecified");
    xmlWriter.writeTextElement("truncated", "0");
    xmlWriter.writeTextElement("difficult", "0");
    xmlWriter.writeStartElement("bndbox");
    xmlWriter.writeTextElement("xmin", "");
    xmlWriter.writeTextElement("ymin", "");
    xmlWriter.writeTextElement("xmax", "");
    xmlWriter.writeTextElement("ymax", "");
    xmlWriter.writeEndElement();//bndbox
    xmlWriter.writeEndElement();//object

    xmlWriter.writeEndElement(); //annotation


    file.close();
}

bool ObjectsRecogFilter::checkDir(QString dirName){
    //To check if a directory named "Folder" exists use:
    return QDir(dirName).exists();
}

void ObjectsRecogFilter::createDir(QString dirName){
    //To create a new folder named "MyFolder" use:
   QDir().mkdir(dirName);
}

std::string ObjectsRecogFilter::getDay(){
        time_t currentTime;
        struct tm *localTime;

        time(&currentTime);
        localTime = localtime(&currentTime);
        int Day = localTime->tm_mday;
        int Month = localTime->tm_mon + 1;
        int Year = localTime->tm_year + 1900;
//        int Hour = localTime->tm_hour;
//        int Min = localTime->tm_min;
//        int Sec = localTime->tm_sec;

        std::string dateTime = std::to_string(Year) + "_"
                + std::to_string(Month) + "_"
                + std::to_string(Day);
        return dateTime;
        //TODO save dir
        //DirectoryInfo directoryR = Directory::CreateDirectory("C:\\Users\\Desktop\\" + dateTime);
}
