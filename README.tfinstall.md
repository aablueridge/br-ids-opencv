## Summary


Compile Tensorflow 2.0 C++ API from source on a NVIDIA system

## [](https://github.com/zhouxf53/tf_cpp_jetson_xavier#hardware-requirement)Hardware requirement


- NVIDIA GPU, CUDA and cuDNN *optional for GPU Tensorflow
- Host computer running Ubuntu Linux x64 Version 18.04 or 16.04
- Internet connection to both system

## Let’s start!

To be able to get there, you will need to do the following:


1. Install some prerequisites
2. Build TensorFlow 2.0 from source
3. Build some other dependencies
4. Create a project and configure it
5. Compile a test project to validate everything is installed properly

## 1. Install some prerequisites

### Install bazel


1. Go to this [URL](https://github.com/bazelbuild/bazel/releases) and search for the relevant OS you have. For MacOS this should be bazel-0.24.1-installer-darwin-x86_64.sh (or a newer version which means 0.24.1 will be different)
2. Download this file to your home directory and open terminal
3. Run the two following commands (assuming you are the home directory and the sh file is there):
```
chmod +x bazel-0.24.1-installer-darwin-x86_64.sh./bazel-0.24.1-installer-darwin-x86_64.sh — user
(again, if you have a different version change the commands)
```

4. Check that bazel is working:
```
bazel version
```
If it is not, check your path, it might be missing $(HOME)/bin

## Install a few more packages:

Brew install autoconf automake libtool cmake

## Download and install TensorFlow

First create a folder where you want to keep all the code. I will show you how to separate your project from the framework, so select a structure that will not confuse you.

I have a main folder under my home that I named “Code” and under it I have my projects. I created a new folder under that, that is called Tensorflow2. There I have the framework code.

### Download the framework code
1. Open terminal and cd to that directory
2. Download the source files:

    ```
    git clone https://github.com/tensorflow/tensorflow.git
    cd tensorflow
    ```
3. This will get you code from Master. If you want a release branch (which at this time is not a release yet) you can check it out. Example:
```
git checkout r2.0
```

### Configure

1. At the same place in terminal (under the tensorflow directory — this is your root folder) run

    ```
    ./configure
    ```

2. Now comes the part where the script asks you questions, and you need to answer

3. It will tell you if it found bazel and what version, and will ask for the python location.

4. The easiest way to find it is to open a new Terminal windows and to type

        which python3


(replace python3 with whatever you use to run python, run “python3 –version” to see what version it is)


5. Then it will ask you a bunch of other questions about special support for various features. I selected the default for all apart from the one.


6. That one is about CPU features. It will ask you for “optimization flags to use during compilation” with bazel. This is important for two reasons:

    a. It will make your models run faster

    b. It will eliminate some warnings that will be displayed every time you run your code.

7. I found out about this only after being able to run my project, so I had to go back and run configure again and type “-mavx -mavx2 -mfma -msse4.2” when prompted for these flags. Not sure how you can find that out up front, try to look [here](https://stackoverflow.com/questions/41293077/how-to-compile-tensorflow-with-sse4-2-and-avx-instructions) or [here](https://github.com/lakshayg/tensorflow-build). So I recommend that at first leave it as default and if you get the runtime warnings, go back and do the configuration again, and compile the framework again.

8. Finish the configuration process and if you have a GPU — read [this](https://www.tensorflow.org/install/source#configuration_options).

###

The following options was used by mine, pay special attention to: CUDA version: 10.0 cuDNN location: /usr/lib/aarch64-linux-gnu compute capability: 7.2 for xavier
```
Please specify the location of python. [Default is /usr/bin/python]: /usr/bin/python3


Found possible Python library paths:
  /usr/local/lib/python3.5/dist-packages
  /usr/lib/python3/dist-packages
Please input the desired Python library path to use.  Default is [/usr/local/lib/python3.5/dist-packages]

Do you wish to build TensorFlow with jemalloc as malloc support? [Y/n]: n

Do you wish to build TensorFlow with Google Cloud Platform support? [Y/n]: n

Do you wish to build TensorFlow with Hadoop File System support? [Y/n]: n

Do you wish to build TensorFlow with Amazon S3 File System support? [Y/n]: n

Do you wish to build TensorFlow with Apache Kafka Platform support? [Y/n]: n

Do you wish to build TensorFlow with XLA JIT support? [y/N]: n

Do you wish to build TensorFlow with GDR support? [y/N]: n

Do you wish to build TensorFlow with VERBS support? [y/N]: n

Do you wish to build TensorFlow with OpenCL SYCL support? [y/N]: n

Do you wish to build TensorFlow with CUDA support? [y/N]: y

Please specify the CUDA SDK version you want to use, e.g. 7.0. [Leave empty to default to CUDA 9.0]: 10.0

Please specify the location where CUDA 9.0 toolkit is installed. Refer to README.md for more details. [Default is /usr/local/cuda]:

Please specify the cuDNN version you want to use. [Leave empty to default to cuDNN 7.0]: 7.3.2

Please specify the location where cuDNN 7 library is installed. Refer to README.md for more details. [Default is /usr/local/cuda]:
/usr/lib/aarch64-linux-gnu

Do you wish to build TensorFlow with TensorRT support? [y/N]: n
TensorRT support will be enabled for TensorFlow.

Please specify the location where TensorRT is installed. [Default is /usr/lib/aarch64-linux-gnu]:

Please specify the NCCL version you want to use. [Leave empty to default to NCCL 1.3]: 1.3


Please specify a list of comma-separated Cuda compute capabilities you want to build with.
You can find the compute capability of your device at: https://developer.nvidia.com/cuda-gpus.
Please note that each additional compute capability significantly increases your build time and binary size. [Default is: 3.5,5.2] 7.2

Do you want to use clang as CUDA compiler? [y/N]: n
nvcc will be used as CUDA compiler.

Please specify which gcc should be used by nvcc as the host compiler. [Default is /usr/bin/gcc]:

Do you wish to build TensorFlow with MPI support? [y/N]: n

Please specify optimization flags to use during compilation when bazel option "--config=opt" is specified [Default is -march=native]:

Would you like to interactively configure ./WORKSPACE for Android builds? [y/N]:
Not configuring the WORKSPACE for Android builds.

Preconfigured Bazel build configs. You can use any of the below by adding "--config=&lt;&gt;" to your build command. See tools/bazel.rc for more details.
	--config=mkl         	# Build with MKL support.
	--config=monolithic  	# Config for mostly static monolithic build.
Configuration finished
```
### Compile the framework

Finally, we get to the point where we can start building things. Let’s build the framework libraries that we will link into the project later.

We need to build the C++ API and the framework.

Important: the next two commands will take several hours to finish. It will maximize your CPU usage and drain your battery. Make sure to be hooked up and take a long break.

At the root folder run:

```
bazel build -c opt — verbose_failures //tensorflow:libtensorflow_cc.so
```
Next compile the framework:

bazel build -c opt — verbose_failures //tensorflow:libtensorflow_framework.so

If these two succeed with not errors (there are many warnings, that’s normal), you will find the two libraries in the folder (root folder)/bazel-bin/tensorflow/. We will need these to link with our project.

If you remember, we configured first for CPU features and that should have made this build optimized for your personal case. However, if you get the CPU features error such as in the troubleshooting section below, try and re-run the framework build again with the command-line bazel options that matches the flags you need. In my case I should run:
```
bazel build -c opt — copt=-mavx — copt=-mavx2 — copt=-mfma — copt=-msse4.2 — verbose_failures //tensorflow:libtensorflow_framework.so
```
## Get and build dependencies

We need a few more things to download before we create our project.

Run this from the root folder:
```
tensorflow/contrib/makefile/download_dependencies.sh
```
It will download files into tensorflow/contrib/makefile/downloads/

### Build protobufs

Run the following:

```
cd tensorflow/contrib/makefile/downloads/protobuf/
./autogen.sh
./configure
make
make install
```

### Build Eigen

Run the following:

```
cd ../eigen
mkdir build_dir
cd build_dir
cmake -DCMAKE_INSTALL_PREFIX=/tmp/eigen/ ../
make install
cd ../../../../../..
```

### 3.5 Install tensorflow library

make directory

```
sudo mkdir /usr/local/tensorflow
sudo mkdir /usr/local/tensorflow/include
```
copy the header files

```
sudo cp -r tensorflow/contrib/makefile/downloads/eigen/Eigen /usr/local/tensorflow/include/
sudo cp -r tensorflow/contrib/makefile/downloads/eigen/unsupported /usr/local/tensorflow/include/
sudo cp -r tensorflow/contrib/makefile/downloads/absl/absl /usr/local/tensorflow/include/
# if your protobuf is 3.6.0 by changing the download file
sudo cp -r tensorflow/contrib/makefile/gen/protobuf/include/google /usr/local/tensorflow/include/
sudo cp tensorflow/contrib/makefile/downloads/nsync/public/* /usr/local/tensorflow/include/
sudo cp -r bazel-genfiles/tensorflow /usr/local/tensorflow/include/
sudo cp -r tensorflow/cc /usr/local/tensorflow/include/tensorflow
sudo cp -r tensorflow/core /usr/local/tensorflow/include/tensorflow
sudo mkdir /usr/local/tensorflow/include/third_party
sudo cp -r third_party/eigen3 /usr/local/tensorflow/include/third_party/
```

Copy library files

```
sudo mkdir /usr/local/tensorflow/lib
sudo cp bazel-bin/tensorflow/libtensorflow_*.so /usr/local/tensorflow/lib
```


### [](https://github.com/zhouxf53/tf_cpp_jetson_xavier#4-testing-tensorflow-c-api)4. Testing tensorflow C++ API

Try a sample C++ file like main.cc provided by [source](https://github.com/hemajun815/tutorial/blob/master/tensorflow/training-a-DNN-using-only-tensorflow-cc.md)

```
#include "tensorflow/cc/client/client_session.h"
#include "tensorflow/cc/ops/standard_ops.h"
#include "tensorflow/cc/framework/gradients.h"

int main()
{
    // benchmark
    auto benchmark_w = 2.0, benchmark_b = 0.5;

    // data
    auto nof_samples = 100;
    struct Sample
    {
        float sample;
        float label;
    };
    std::vector&lt;struct Sample&gt; dataset;
    std::srand((unsigned)std::time(NULL));
    for (int i = 0; i &lt; nof_samples; i++)
    {
        float sample = std::rand() / float(RAND_MAX) - 0.5;
        float label = benchmark_w * sample + benchmark_b + std::rand() / float(RAND_MAX) * 0.01;
        dataset.push_back({sample, label});
    }

    // model
    tensorflow::Scope root = tensorflow::Scope::NewRootScope();
    auto x = tensorflow::ops::Placeholder(root, tensorflow::DataType::DT_FLOAT);
    auto y = tensorflow::ops::Placeholder(root, tensorflow::DataType::DT_FLOAT);
    auto w = tensorflow::ops::Variable(root, {1, 1}, tensorflow::DataType::DT_FLOAT);
    auto assign_w = tensorflow::ops::Assign(root, w, tensorflow::ops::RandomNormal(root, {1, 1}, tensorflow::DataType::DT_FLOAT));
    auto b = tensorflow::ops::Variable(root, {1, 1}, tensorflow::DataType::DT_FLOAT);
    auto assign_b = tensorflow::ops::Assign(root, b, {{0.0f}});
    auto y_ = tensorflow::ops::Add(root, tensorflow::ops::MatMul(root, x, w), b);
    auto loss = tensorflow::ops::L2Loss(root, tensorflow::ops::Sub(root, y_, y));
    std::vector&lt;tensorflow::Output&gt; grad_outputs;
    TF_CHECK_OK(AddSymbolicGradients(root, {loss}, {w, b}, &grad_outputs));
    auto learn_rate = tensorflow::ops::Const(root, 0.01f, {});
    auto apply_w = tensorflow::ops::ApplyGradientDescent(root, w, learn_rate, {grad_outputs[0]});
    auto apply_b = tensorflow::ops::ApplyGradientDescent(root, b, learn_rate, {grad_outputs[1]});

    // train
    tensorflow::ClientSession sess(root);
    sess.Run({assign_w, assign_b}, nullptr);
    std::vector&lt;tensorflow::Tensor&gt; outputs;
    timespec t0, t1;
    clock_gettime(CLOCK_MONOTONIC, &t0);
    for (int epoch = 1; epoch &lt;= 64; epoch++)
    {
        std::random_shuffle(dataset.begin(), dataset.end());
        for (int i = 0; i &lt; nof_samples; i++)
        {
            TF_CHECK_OK(sess.Run({{x, {{dataset[i].sample}}}, {y, {{dataset[i].label}}}}, {w, b, loss, apply_w, apply_b}, &outputs));
        }
        LOG(INFO) &lt;&lt; "epoch " &lt;&lt; epoch &lt;&lt; ": w=" &lt;&lt; outputs[0].matrix&lt;float&gt;() &lt;&lt; " b=" &lt;&lt; outputs[1].matrix&lt;float&gt;() &lt;&lt; " loss=" &lt;&lt; outputs[2].scalar&lt;float&gt;();
    }
    clock_gettime(CLOCK_MONOTONIC, &t1);
    LOG(INFO) &lt;&lt; "elapsed time： " &lt;&lt; t1.tv_sec - t0.tv_sec + (t1.tv_nsec - t0.tv_nsec) * 1.0 / 1000000000 &lt;&lt; "s";
    return 0;
}
```
or official example.cc [source](https://www.tensorflow.org/guide/extend/cc)

```
// tensorflow/cc/example/example.cc

#include "tensorflow/cc/client/client_session.h"
#include "tensorflow/cc/ops/standard_ops.h"
#include "tensorflow/core/framework/tensor.h"

int main() {
  using namespace tensorflow;
  using namespace tensorflow::ops;
  Scope root = Scope::NewRootScope();
  // Matrix A = [3 2; -1 0]
  auto A = Const(root, { {3.f, 2.f}, {-1.f, 0.f} });
  // Vector b = [3 5]
  auto b = Const(root, { {3.f, 5.f} });
  // v = Ab^T
  auto v = MatMul(root.WithOpName("v"), A, b, MatMul::TransposeB(true));
  std::vector&lt;Tensor&gt; outputs;
  ClientSession session(root);
  // Run and fetch v
  TF_CHECK_OK(session.Run({v}, &outputs));
  // Expect outputs[0] == [19; -3]
  LOG(INFO) &lt;&lt; outputs[0].matrix&lt;float&gt;();
  return 0;
}
```

Both files can be compiled from any locations:
```
g++ -std=c++11 ./main.cc -ltensorflow_framework -ltensorflow_cc -o tfcc \
-I/usr/local/tensorflow/include/ \
-L/usr/local/tensorflow/lib/ \
-Wl,-rpath=/usr/local/tensorflow/lib/
```
