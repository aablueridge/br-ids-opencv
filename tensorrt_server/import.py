# Parse Imports
import cv2 as cv
import numpy as np
from xml.etree import ElementTree as et
from lxml import etree
from pathlib import Path
from collections import namedtuple


# App Imports
import os
import requests
import json, argparse
import datetime

# Jetson Imports
import glob
import argparse
import sys
import time 


#from utils.camera import add_camera_args, Camera
#from utils.display import open_window, set_display, show_fps
from pytrt import PyTrtGooglenet
import os

PIXEL_MEANS = np.array([[[55.833648681640625, 57.65956115722656, 80.56794738769531]]], dtype=np.float32)
DEPLOY_ENGINE = 'googlenet/deploy.engine'
ENGINE_SHAPE0 = (3, 224, 224)
ENGINE_SHAPE1 = (9, 1, 1)
RESIZED_SHAPE = (224, 224)
labels = np.loadtxt('googlenet/synset_words.txt', str, delimiter='\t')
net = PyTrtGooglenet(DEPLOY_ENGINE, ENGINE_SHAPE0, ENGINE_SHAPE1)

def save_crop(xmin, xmax, ymin, ymax, im):


    im_crop = im[int(ymin):int(ymax), int(xmin):int(xmax)]
    height = im_crop.shape[0]
    width = im_crop.shape[1]
    channels = im_crop.shape[2]


    #print('Image Height       : ',height)
    #print('Image Width        : ',width)
    #print('Number of Channels : ',channels)

    #print("X0: ", xmin)
    #print("X1: ", xmax)
    #print("Y0: ", ymin)
    #print("Y1: ", ymax)
    return im_crop

def parse_file(h,w,xmin,xmax,ymin,ymax, img):

    # Get widths of box
    bw = xmax - xmin
    bh = ymax - ymin

    # Get center of box
    bcx = xmin + bw/2
    bcy = ymin + bh/2

    # Pad boundry condition
    p = 50
    c_delta = max(bw,bh)/2
    pxmin = p + c_delta
    pxmax = w - pxmin
    pymin = pxmin
    pymax = h - pymin

     # assign center based on boundry conditions of x
    if (bcx < pxmin):
        cx = pxmin
    elif  (bcx > pxmax):
        cx = pxmax
    else:
        cx = bcx

    # assign center based on boundry conditions of y
    if (bcy < pymin):
        cy = pymin
    elif (bcy > pymax):
        cy  = pymax
    else:
        cy = bcy

    # New BBox cordinates
    delta = p + c_delta
    x0 = cx - delta
    x1 = cx + delta
    y0 = cy - delta
    y1 = cy + delta

    return save_crop(x0, x1, y0, y1, img)

qbfile = open("folders.txt", "r")

for directory in qbfile:
    directory = directory.rstrip()
    # Loop through files in directory
    for filename in os.listdir(directory):

        # Try & catch so loop doesn't break
       	#try:

        # Check if filename ends with XML extension
        if filename.endswith(".xml"):

            # Split name and extension
            f_name, file_extension = os.path.splitext(filename)

            # Filepath to save the casino
            file_path=(os.path.join(directory, f_name))

            # Get xml tree and the root to pharse doc
            tree = et.parse(os.path.join(directory, filename))
            root = tree.getroot()

            # Get Size
            size = root.find('size')
            folder = root.find('folder')
            w = size.find('width')
            h = size.find('height')

            w = float(w.text)
            h = float(h.text)
            folder =  folder.text
            img = cv.imread(os.path.join(directory, (f_name+".jpg")))

            # Get Time & Date (Split Filename)

            # Iterate through all detection objects
            for obj in root.findall('object'):
                #name = obj.find('name')
                bndbox = obj.find('bndbox')
                #print (name.text)
                xmin = bndbox.find('xmin')
                ymin = bndbox.find('ymin')
                xmax = bndbox.find('xmax')
                ymax = bndbox.find('ymax')

                # Turn image into square aspect ratio
                #if w is 800 +-50
                xmin = int(float(xmin.text))
                ymin = int(float(ymin.text))
                xmax = int(float(xmax.text))
                ymax = int(float(ymax.text))
                
                # get crop
                crop =  parse_file(h,w,xmin,xmax,ymin,ymax, img)
                
                # convert input
                inp = cv.resize(crop, RESIZED_SHAPE)
                inp = inp.astype(np.float32) - PIXEL_MEANS
                inp = inp.transpose((2, 0, 1))  # HWC -> CHW

                # Do detection 
                out = net.forward(inp[None])
                out_prob = np.squeeze(out['prob'][0])
                top_inds = out_prob.argsort()[::-1][:1]
                #print(labels[top_inds])
                detection = labels[top_inds][0]
                
				# check for dir
                save_path = "/xavier_ssd/crop/"+ folder + "/" + detection
                if not os.path.exists(save_path):
                    os.makedirs(save_path)
                
                # Write crop
                cv.imwrite((save_path + "/" + f_name + "_crop.jpg"), crop )
                
                #orginal_image = os.path.join(directory, (f_name+".jpg"))
                #copy_location = "/xavier_ssd/crop/"+ detection + "/" + f_name + ".jpg"
                #os.symlink(orginal_image, copy_location)
                #	

         #except:
         #    print("error")
