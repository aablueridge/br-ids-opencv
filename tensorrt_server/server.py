# Parse Imports
import cv2 as cv
import numpy as np
from xml.etree import ElementTree as et
from lxml import etree
from pathlib import Path
from collections import namedtuple


# App Imports
import os
import requests
import json, argparse
import datetime

# Jetson Imports
import glob
import argparse
import sys
import time 


#from utils.camera import add_camera_args, Camera
#from utils.display import open_window, set_display, show_fps
from pytrt import PyTrtGooglenet
import os

PIXEL_MEANS = np.array([[[40.410736083984375, 40.11534118652344, 54.27168273925781]]], dtype=np.float32)
DEPLOY_ENGINE = 'googlenet/deploy.engine'
ENGINE_SHAPE0 = (3, 224, 224)
ENGINE_SHAPE1 = (4, 1, 1)
RESIZED_SHAPE = (224, 224)
labels = np.loadtxt('googlenet/synset_words.txt', str, delimiter='\t')
net = PyTrtGooglenet(DEPLOY_ENGINE, ENGINE_SHAPE0, ENGINE_SHAPE1)
from flask import Flask, request
from flask_cors import CORS


app = Flask(__name__)
cors = CORS(app)


def save_crop(xmin, xmax, ymin, ymax, im):


	im_crop = im[int(ymin):int(ymax), int(xmin):int(xmax)]
	height = im_crop.shape[0]
	width = im_crop.shape[1]
	channels = im_crop.shape[2]
	
	
	print('Image Height       : ',height)
	print('Image Width        : ',width)
	print('Number of Channels : ',channels)
	
	print("X0: ", xmin)
	print("X1: ", xmax)
	print("Y0: ", ymin)
	print("Y1: ", ymax)
	return im_crop

def parse_file(data, img):

	# img data
	h = float(data['height'])
	w = float(data['width'])

	# bbox info
	xmin = int(float(data['xmin']))
	ymin = int(float(data['ymin']))
	xmax = int(float(data['xmax']))
	ymax = int(float(data['ymax']))

	# Get widths of box
	bw = xmax - xmin
	bh = ymax - ymin

	# Get center of box
	bcx = xmin + bw/2
	bcy = ymin + bh/2

	# Pad boundry condition
	p = 50
	c_delta = max(bw,bh)/2
	pxmin = p + c_delta
	pxmax = w - pxmin
	pymin = pxmin
	pymax = h - pymin

	 # assign center based on boundry conditions of x
	if (bcx < pxmin):
		cx = pxmin
	elif  (bcx > pxmax):
		cx = pxmax
	else:
		cx = bcx

	# assign center based on boundry conditions of y
	if (bcy < pymin):
		cy = pymin
	elif (bcy > pymax):
		cy  = pymax
	else:
		cy = bcy

	# New BBox cordinates
	delta = p + c_delta
	x0 = cx - delta
	x1 = cx + delta
	y0 = cy - delta
	y1 = cy + delta

	return save_crop(x0, x1, y0, y1, img)

@app.route("/post", methods=['POST'])
def accept_data():
	print("got data")
	data = request.data.decode("utf-8")
	print(data)
	
	params = json.loads(data)
	img = cv.imread(os.path.join(params['path'], params['jpg']))

	crop = parse_file(params,img)
	
	#better crop function 
	cv.imwrite( str(params['path']+"/crop/" + params['jpg']), crop )
	
	crop = cv.resize(crop, RESIZED_SHAPE)
	crop = crop.astype(np.float32) - PIXEL_MEANS
	crop = crop.transpose((2, 0, 1))  # HWC -> CHW


	out = net.forward(crop[None])
	out_prob = np.squeeze(out['prob'][0])
	top_inds = out_prob.argsort()[::-1][:1]
	print(labels[top_inds])	

	return ""

app.debug = True

# Run the main app
if __name__ == '__main__':

	app.run(host='0.0.0.0', threaded=True)
