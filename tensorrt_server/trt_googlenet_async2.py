import sys
import time
import argparse
import threading

import numpy as np
import cv2
from utils.camera import add_camera_args, Camera
from utils.display import open_window, set_display, show_fps
from pytrt import PyTrtGooglenet
import os

PIXEL_MEANS = np.array([[[40.410736083984375, 40.11534118652344, 54.27168273925781]]], dtype=np.float32)
DEPLOY_ENGINE = 'googlenet/deploy.engine'
ENGINE_SHAPE0 = (3, 224, 224)
ENGINE_SHAPE1 = (4, 1, 1)
RESIZED_SHAPE = (224, 224)
labels = np.loadtxt('googlenet/synset_words.txt', str, delimiter='\t')

net = PyTrtGooglenet(DEPLOY_ENGINE, ENGINE_SHAPE0, ENGINE_SHAPE1)


 # preprocess the image crop
crop = cv2.imread('/xavier_ssd/pyserver/crop/defect/defects/202041_2919_nobox.jpg')
crop = cv2.resize(crop, RESIZED_SHAPE)
crop = crop.astype(np.float32) - PIXEL_MEANS
crop = crop.transpose((2, 0, 1))  # HWC -> CHW


out = net.forward(crop[None])
out_prob = np.squeeze(out['prob'][0])
top_inds = out_prob.argsort()[::-1][:1]
labels[top_inds]


directory = "/xavier_ssd/pyserver/crop/defect/defects/"
for filename in os.listdir(directory):
	if filename.endswith(".jpg"): 
		img = cv2.imread(os.path.join(directory, filename))
		crop = img
		crop = cv2.resize(crop, RESIZED_SHAPE)
		crop = crop.astype(np.float32) - PIXEL_MEANS
		crop = crop.transpose((2, 0, 1))  # HWC -> CHW
		out = net.forward(crop[None])
		out_prob = np.squeeze(out['prob'][0])
		top_inds = out_prob.argsort()[::-1][:1]
		labels[top_inds]




