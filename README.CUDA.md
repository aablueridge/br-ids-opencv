#### Install Python and the TensorFlow package dependencies
```
sudo apt install python-dev python-pip python3-dev python3-pip

```

#### Install the TensorFlow pip package dependencies (if using a virtual environment, omit the --user argument):
```
pip install -U --user pip six numpy wheel setuptools mock 'future>=0.17.1'
pip install -U --user keras_applications==1.0.6 --no-deps
pip install -U --user keras_preprocessing==1.0.5 --no-deps
```


#### Install Bazel

[Install Bazel](https://docs.bazel.build/versions/master/install.html), the build tool used to compile TensorFlow.

Add the location of the Bazel executable to your PATH environment variable.
#### Install CUDA with apt

This section shows how to install CUDA 10 (TensorFlow &gt;= 1.13.0) and CUDA 9 for Ubuntu 16.04 and 18.04. These instructions may work for other Debian-based distros.

**Caution:** [Secure Boot](https://wiki.ubuntu.com/UEFI/SecureBoot) complicates installation of the NVIDIA driver and is beyond the scope of these instructions.

##### Ubuntu 18.04 (CUDA 10)
```
# Add NVIDIA package repositories
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-repo-ubuntu1804_10.0.130-1_amd64.deb
sudo dpkg -i cuda-repo-ubuntu1804_10.0.130-1_amd64.deb
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub
sudo apt-get update
wget http://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64/nvidia-machine-learning-repo-ubuntu1804_1.0.0-1_amd64.deb
sudo apt install ./nvidia-machine-learning-repo-ubuntu1804_1.0.0-1_amd64.deb
sudo apt-get update
```

```
# Install NVIDIA driver
sudo apt-get install --no-install-recommends nvidia-driver-430
```
